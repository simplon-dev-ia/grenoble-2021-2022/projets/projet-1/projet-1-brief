![Open Food Facts Nutri-score Logo](openfoodfacts-nutriscore.png)

# Projet 1 - Nutri-score

> Prédiction de l'indicateur alimentaire nutri-score

## Contexte du projet

L'équipe IA de l'association [Open Food Facts](https://fr.openfoodfacts.org) fait appel à vous !

Beaucoup de produits de leur base de données sont déjà annotés avec l'indicateur
[nutri-score](https://www.santepubliquefrance.fr/determinants-de-sante/nutrition-et-activite-physique/articles/nutri-score),
mais il en manque encore. Ils se demandent si cet indicateur ne pourrait pas être inféré automatiquement
à partir des autres méta-données présentes sur chaque produit.

Vous êtes en charge de développer un prototype d'application web permettant de prédire le nutri-score
d'un produit à partir d'une série de critères à définir. L'application devra à minima proposer un formulaire
mais il est envisagé de développer une API REST permettant d'automatiser le processus.

## Ressources

- [Données Open Food Facts](https://fr.openfoodfacts.org/data)

## Livrables

- Dépôt GitLab (pas de branches nominatives !)
- Code source fonctionnel (Bash, Python, Jupyter, Docker)
- Documents de réflexion et de conception
    - Maquettes
    - Schémas d'architecture technique
    - Schémas du modèle de données
    - Sources documentaires utilisées
- L'outil de suivi d'avancement du projet

## Modalités pédagogiques

Le projet est à réaliser par groupe de 2 apprenants sur environ 2 semaines, du 05 au 21 janvier 2022.

Une méthodologie Agile de type Scrum sera mise en place avec 2 sprints de 1 semaine chacun.

Les rendez-vous projets :

- Mercredi 5 janvier : lancement du projet
- Mercredi 12 janvier : point d'étape 1
- Mercredi 19 janvier : point d'étape 2
- Mercredi 26 janvier : présentation projet

## Critères de performance

Le but de ce premier projet est de mettre en pratique les aspects suivants :

- Mise en place d'un pipeline ETL sans base de données (extraction et nettoyage)
- Manipulation de données avec des _DataFrames_
- Choix, entrainement, validation et inférence d'un modèle de Machine Learning
- Abstraction de la couche fonctionnelle d'un modèle de Machine Learning
- Mise en place d'une application web avec formulaire simple
- Déploiement d'une application web Python / Docker sur Azure App Service
- Initiation à la gestion de projet Agile de type Scrum

Technologies suggérées :

- Languages de programmation : Bash, Python
- Analyse de données : Jupyter, Pandas, Matplotlib, Seaborn
- Machine Learning : Scikit-Learn
- Web : Flask, Django, FastAPI
- Déploiement : Azure App Service

## Modalités d'évaluation

La présentation finale du projet sera effectuée en équipe le 26 janvier 2021 :

- 15 minutes de présentation
- 15 minutes d'échange

La présentation devra notamment exposer votre analyse des problèmes et les solutions développées pour y répondre.
Egalement, un retour d'expérience sur le travail collaboratif en méthodologie Agile Scrum est attendu.

## Compétences visées

- C1. Qualifier les données grâce à des outils d’analyse et de visualisation de données en vue de vérifier leur adéquation avec le projet. (Niveau 1)
- C3. Programmer l’import de données initiales nécessaires au projet en base de données, afin de les rendre
exploitables par un tiers, dans un langage de programmation adapté et à partir de la stratégie de nettoyage
des données préalablement définie. (Niveau 1)
- C5. Concevoir le programme d’intelligence artificielle adapté aux données disponibles afin de répondre aux
objectifs fonctionnels du projet, à l’aide des algorithmes, outils et méthodes standards, notamment de
machine learning et de deep learning. (Niveau 1)
- C6. Développer le programme d’intelligence artificielle selon les données du projet et les éléments de
conception définis, en exploitant les algorithmes et les outils standards couramment utilisés dans le domaine. (Niveau 1)
- C7. Développer l’interaction entre les fonctionnalités de l’application et l’intelligence artificielle dans le respect des objectifs visés et des bonnes pratiques du domaine. (Niveau 1)
- C9. Analyser un besoin en développement d’application mettant en oeuvre des techniques d'intelligence
artificielle afin de produire les éléments de réponses techniques. (Niveau 1)
- C12. Développer le back-end de l’application d’intelligence artificielle dans le respect des spécifications
fonctionnelles et des bonnes pratiques du domaine. (Niveau 1)
- C13. Développer le front-end de l’ application d’intelligence artificielle à partir de maquettes et du parcours
utilisateur⋅rice, dans le respect des objectifs visés et des bonnes pratiques du domaine. (Niveau 1)
- C16. Planifier les actions du projet à l’aide d’un outil adapté afin de prévoir la complétion du projet dans les
temps impartis. (Niveau 1)
- C17. Concevoir un système de veille technologique permettant de collecter, classifier et analyser l’information afin d’améliorer la prise de décisions techniques. (Niveau 1)
- C18. Communiquer avec les parties prenantes afin de rendre compte de l'avancement du projet en mettant en
oeuvre les canaux de communication nécessaires. (Niveau 1)
